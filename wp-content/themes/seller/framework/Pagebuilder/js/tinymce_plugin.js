(function() {

	if( typeof tinymce!=='undefined' ){


		tinymce.create(
			"tinymce.plugins.themeton_shortcode",
			{
				init: function(ed,e) { },
				createControl:function(d,e)
				{

					var ed = tinymce.activeEditor;

					if(d=="themeton_shortcode"){

						d=e.createMenuButton( "themeton_shortcode",{
							title: "Themeton Shortcode",
							icons: false
							});

							var a=this;d.onRenderMenu.add(function(c,b){

								/*
								b.add({title : 'Some item 1', onclick : function() {
									alert('Some item 1 was clicked.');
			                    }});
								*/
								a.addImmediate(b, "Alert Box", '[blox_notification title="" type="alert-success" alignment="left" skin="boxed"]Content[/blox_notification]' );
								a.addImmediate(b, "Button", '[blox_button text="" link="#" target="_self" button_type="btn-default" icon="" size="btn-md" /]' );
								a.addImmediate(b, "Divider", '[blox_divider type="space" space="5" /]' );
								a.addImmediate(b, "Dropcap", '[blox_dropcap]Character[/blox_dropcap]' );
								a.addImmediate(b, "Highlight Text", '[blox_highlight type="default|primary|success|info|warning|danger"]Text[/blox_highlight]' );
								a.addImmediate(b, "Icon", '[blox_icon icon="fa-user" color="#1e73be" /]' );
								a.addImmediate(b, "Iconic List", '[blox_list title="Title" icon="fa-thumbs-o-up" color="#1e73be"]<ul><li>item 1</li><li>item 2</li></ul>[/blox_list]' );
								a.addImmediate(b, "Tooltip", '[blox_tooltip tooltip="Tooltip text"]Text[/blox_tooltip]' );



								b.addSeparator();

								c=b.addMenu({title: "Media Shortcodes"});
										a.addImmediate(c, "Audio", '[blox_audio title="" image=""]audio_url_or_embed[/blox_audio]' );
										a.addImmediate(c, "Video", '[blox_video title="" image=""]video_url_or_embed[/blox_video]' );

							});
						return d

					} 

					return null
				},
				addImmediate:function(d,e,a){d.add({title:e,onclick:function(){tinyMCE.activeEditor.execCommand( "mceInsertContent",false,a)}})}
			}
		);

		tinymce.PluginManager.add( "themeton_shortcode", tinymce.plugins.themeton_shortcode);

	}

})();